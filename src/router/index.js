import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Home'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/admin',
    name: 'Admin',
    component: () => import(/* webpackChunkName: "about" */ '../components/Admin')
  },
  {
    path: '/deal',
    name: 'Deal',
    component: () => import(/* webpackChunkName: "about" */ '../components/Deals')
  },
  {
    path: '/analytics',
    name: 'Analytics',
    component: () => import(/* webpackChunkName: "about" */ '../components/Analytics')
  },
  {
    path: '/articles',
    name: 'Articles',
    component: () => import(/* webpackChunkName: "about" */ '../components/lists/Articles')
  },
  {
    path: '/temp-docs',
    name: 'tempDocs',
    component: () => import(/* webpackChunkName: "about" */ '../components/lists/tempDocs')
  },
  {
    path: '/documents',
    name: 'Documents',
    component: () => import(/* webpackChunkName: "about" */ '../components/lists/Documents')
  },
  {
    path: '/edit',
    name: 'Edit',
    component: () => import(/* webpackChunkName: "about" */ '../components/Edit')
  },
  {
    path: '/settings',
    name: 'Settings',
    component: () => import(/* webpackChunkName: "about" */ '../components/Settings')
  },
  {
    path: '/auth',
    name: 'Auth',
    component: () => import(/* webpackChunkName: "about" */ '../components/Auth')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
